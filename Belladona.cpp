#include "Belladona.h"
#include "OrganismFactory.h"
#include "World.h"
static Factory::Registrar<Belladona> REGISTRAR("Belladona");

void Belladona::collision(bool isAttacking, Organism* other)
{
	this->kill();
	this->moveIn2D(position);
	other->kill();
	other->moveIn2D(other->getPosition());
	worldPtr->worldEvents()->addEatEvent(other->getID(), organismID);
}

