﻿#pragma once
#include "Printer.h"
#include <memory>
#include <iostream>
class World; //forward declaration is needed

class Organism
{

public:
	void setWorld(World* world); 
	void increaseStrength(int toAdd); //niepotrzebne majac set
	void kill();	//ustawia organizm do usuniecia
	void setStrength(const unsigned strength);

	bool isToDeletion() const;
	unsigned getInitiative() const;
	unsigned getStrength() const;
	std::pair<int, int>getPosition() const;
	std::string getID() const;
	
	virtual void action() = 0;
	virtual void collision(bool isAttacking, Organism* other) = 0;
	void drawAtCurrentPos() const;
	void moveIn2D(std::pair<int, int> newPosition);
	
	friend bool operator==(const Organism& current, const Organism& other);
	virtual ~Organism() = default;

protected:
	Organism(const int x, const int y, World* const worldPtr = nullptr);
	void multiplicate();
	std::pair<int, int> closeFreePos();

	unsigned int strength;
	unsigned int initiative;
	char symbol;
	bool toDeletion;
	World* worldPtr;
	std::string organismID;
	std::pair<int, int> position;
	Console::COLORS color;
	Console::COLORS backgroundColor;
};
