#include "Antelope.h"
#include  "World.h"

static Factory::Registrar<Antelope>REGISTRAR("Antelope");

void Antelope::collision(const bool isAttacking, Organism* other)
{
	Rng::RandomNumberGenerator rng;
	const auto currPosition = position;
	const auto newPosition = closeFreePos();
	if (newPosition.first != -1 && rng.randomBool(0.5))		//jest gdzie uciec i weszlo 50%
	{
		if (isAttacking)
		{
			moveIn2D(newPosition);							//ucieczka brak inicjacji walki
			worldPtr->worldEvents()->escapeEvent(organismID);
		}
		else												//jest obronca
		{
			moveIn2D(newPosition);							//przenosi sie w 2D i aktualizuje pozycje swoja
			other->moveIn2D(currPosition);					//atakujacy przenosi sie i aktualizuje swoja pozycje
			worldPtr->worldEvents()->escapeEvent(organismID);
		}
	}
	else
	{
		if (isAttacking)
		{
			other->collision(false, this);
		}
		else
		{
			Animal::collision(false, other);
		}
	}
}