#include "Mouse.h"
#include "World.h"
static Factory::Registrar<Mouse>REGISTRAR("Mouse");

void Mouse::collision(const bool isAttacking, Organism* other)
{
	const auto newPosition = closeFreePos();
	const auto currPosition = position;
	if (isAttacking)
	{
		other->collision(false, this);
	}
	else if (!isAttacking && newPosition.first != -1)		//mysz sie broni i ma wolna pozycje na ucieczke
	{
		moveIn2D(newPosition);								//mysz przenosi sie na nowa pozycje
		if (other !=nullptr) //TODO:: MAKE IT AS EXCEPTON
		{
			other->moveIn2D(currPosition);
			worldPtr->worldEvents()->escapeEvent(organismID);
		}
	}
	else
	{
		Animal::collision(false, other);			//zwykla walka
	}
}
