﻿#pragma once
#include "Organism.h"

class Plant : public Organism
{
protected:
	Plant(const int x, const int y) : Organism(x, y), probabilityToSpawn(0.17)
	{}
	void action() override;
	void collision(bool isAttacking, Organism* other) override;
	double probabilityToSpawn;	
};

