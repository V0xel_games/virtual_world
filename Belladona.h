#pragma once
#include "Plant.h"
class Belladona : public Plant
{
public:
	~Belladona() = default;
	Belladona(const int x, const int y)	: Plant(x, y)
	{
		organismID = "Belladona";
		symbol = 'B';
		color = Console::LIGHTMAGENTA;
	}
	
protected:
	void collision(bool isAttacking, Organism* other) override;	
};

