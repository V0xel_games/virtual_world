﻿#include "RandomNumberGenerator.h"

namespace Rng
{
	
	bool RandomNumberGenerator::randomBool(const double probability)
	{
		const std::bernoulli_distribution distr(probability);
		return distr(rng);
	}

	int RandomNumberGenerator::randomNumber(const int begin, const int end)
	{
		std::uniform_int_distribution<int>distribution(begin, end);
		return distribution(rng);
	}
}
