﻿#include "Plant.h"
#include "RandomNumberGenerator.h"
#include "World.h"

void Plant::action()
{
	Rng::RandomNumberGenerator rng;
	if (rng.randomBool(probabilityToSpawn))
	{
		multiplicate();
	}
}

void Plant::collision(bool isAttacking, Organism* other)
{
	this->kill();
	other->moveIn2D(position);
	worldPtr->worldEvents()->addEatEvent(other->getID(), organismID);
}
