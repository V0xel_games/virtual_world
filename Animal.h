﻿#pragma once
#include "Organism.h"
#include "OrganismFactory.h"
#include "RandomNumberGenerator.h"

class Animal : public Organism
{
protected:
	Animal(const int x, const int y) : Organism(x, y), movementSize(1)
	{}
	void action() override;
	void collision(bool isAttacking, Organism* other) override;
	unsigned int movementSize;

private:
	std::pair<int, int>findNewPos() const; //znajduje nowa pozycje dla ruchu
};
