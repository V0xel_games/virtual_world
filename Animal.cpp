﻿#include "Animal.h"
#include "World.h"

void Animal::action()
{
	const auto newPosition = findNewPos();
	if(worldPtr->isEmpty(newPosition))			//jezeli na nowej pozycji nic sie nie znajduje
	{
		moveIn2D(newPosition);
		return;
	}
	Organism* otherOrg = worldPtr->getByPosition(newPosition);
	if ((*this)==*otherOrg)
	{
		multiplicate();
		return;
	}	
	this->collision(true, otherOrg); //wywolanie kolizji podajac aktualny jako atakujacego(bo my wywolaismy akcje)	
}

void Animal::collision(const bool isAttacking, Organism* other)
{
	if (isAttacking)								//jezeli jestesmy atakujacym to wywolujemy kolizje drugiego podajac mu ze jest obronca
	{
		other->collision(false, this);
	}
	else											//jestesmy obronca
	{
		if (strength > other->getStrength())		//sila obroncy wieksza od atakuajcego
		{
			other->kill();							//mark atakujacego ze zginal
			other->moveIn2D(other->getPosition());  //usuwamy atakujacego z 2D, obronca zostaje na swoim miejscu
			worldPtr->worldEvents()->addDefenseWinEvent(organismID, other->getID());
		} 
		else										//sila obroncy mniejsza lub rowna
		{
			this->kill();							//obronca mark ze ginie
			other->moveIn2D(position);				//atakujacy przesuwa sie na pozycje obroncy - obronca znika z 2D
			worldPtr->worldEvents()->addAttackWinEvent(other->getID(), organismID);
		}
	}
}

std::pair<int, int> Animal::findNewPos() const
{
	std::vector<std::pair<int, int>> buffor;
	int right = position.first + movementSize; //TODO::mozna by to rozwiazac jako tablica wektorow kierunkowych
	int left = position.first - movementSize;
	int down = position.second + movementSize;
	int up = position.second - movementSize;
	
	if (right<=worldPtr->getMaxPos())
	{
		buffor.emplace_back(right, position.second);
	}
	if (left>=worldPtr->getMinPos())
	{
		buffor.emplace_back(left, position.second);
	}
	if (down<=worldPtr->getMaxPos())
	{
		buffor.emplace_back(position.first, down);
	}
	if (up>=worldPtr->getMinPos())
	{
		buffor.emplace_back(position.first, up);
	}

	Rng::RandomNumberGenerator rng;
	const auto index = rng.randomNumber(0, buffor.size() - 1);
	return buffor[index];
}
