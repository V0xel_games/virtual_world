#include "Logger.h"
namespace Log
{

	Logger::Logger() = default;
	Logger::~Logger() = default;

	void Logger::addAttackWinEvent(const std::string& winner, const std::string& looser)
	{
		logData += winner + " kills " + looser + "\n";
	}

	void Logger::addDefenseWinEvent(const std::string& winner, const std::string& looser)
	{
		logData += looser + " fails attack on " + winner + "\n";
	}

	void Logger::addEatEvent(const std::string& animal, const std::string& plant)
	{
		logData += animal + " eats " + plant + "\n";
	}

	void Logger::addSpawnEvent(const std::string& organism)
	{
		logData += organism + " multiplies " + "\n";
	}

	void Logger::escapeEvent(const std::string& organism)
	{
		logData += organism + " escapes\n";
	}

	void Logger::clear()
	{
		logData.clear();
	}

	std::ostream& operator<<(std::ostream& os, const Logger& data)
	{
		os << "Event log:\n" << data.logData;
		return os;
	}
}
