#pragma once
#include "Animal.h"
class Elephant : public Animal
{
public:
	~Elephant() = default;
	Elephant(const int x, const int y) : Animal(x, y)
	{
		strength = 9;
		initiative = 3;
		organismID = "Elephant";
		symbol = 'E';
		color = Console::BROWN;
	}
	
protected:
	void action() override;
};

