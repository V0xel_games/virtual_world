﻿#pragma once
#include <random>

namespace Rng
{
	class RandomNumberGenerator
	{
	public:
		bool randomBool(double probability);
		int randomNumber(int begin, int end);
	private:
		std::mt19937 rng{ std::random_device{}() }; //seedowanie silnika ranodmizujacego
	};
}
