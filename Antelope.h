#pragma once
#include "Animal.h"
class Antelope : public Animal
{
public:
	~Antelope() = default;
	Antelope(const int x, const int y) : Animal(x, y)
	{
		strength = 4;
		initiative = 4;
		movementSize = 2;
		organismID = "Antelope";
		symbol = 'A';
		color = Console::YELLOW;
	}

protected:
	void collision(bool isAttacking, Organism* other) override;
};

