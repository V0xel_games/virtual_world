﻿#include "Organism.h"
#include "World.h" 

Organism::Organism(const int x, const int y, World* const worldPtr) : toDeletion(false), position(std::make_pair(x, y)), worldPtr(worldPtr), color(Console::BLACK), organismID("Undefined"), symbol('?'),
backgroundColor(Console::LIGHTGREEN), strength(0), initiative(0)
{}

void Organism::setWorld(World*const world) { this->worldPtr = world; }
void Organism::kill(){ toDeletion = true; }
void Organism::setStrength(const unsigned strength) { this->strength = strength; }
void Organism::increaseStrength(const int toAdd) { strength += toAdd; }

bool Organism::isToDeletion() const { return toDeletion; }
unsigned Organism::getInitiative() const { return initiative; }
unsigned Organism::getStrength() const { return strength; }
std::pair<int, int> Organism::getPosition() const { return position; }
std::string Organism::getID() const { return organismID; }

void Organism::drawAtCurrentPos() const
{
	Console::Printer::getInstance().putxy(symbol, color | backgroundColor << 4, position.first+1+35, position.second+1); //przesuniecie bitowe z kolorem tla bo w WINAPI attr jest i kolerem litery i tla - jedne 4 bity litery kolejne 4 tlo 
}

void Organism::moveIn2D(const std::pair<int, int> newPosition)
{
	worldPtr->setNew2DPosition(position, newPosition);			//przesuwamy sie w vectorze2D
	position = newPosition;										//aktualizujemy swoja pozycje
}

std::pair<int, int> Organism::closeFreePos()
{
	auto right = position.first + 1; //TODO:: tablica wektorow kierunkowych
	auto left = position.first - 1;
	auto down = position.second + 1;
	auto up = position.second - 1;

	if (right <= worldPtr->getMaxPos() && worldPtr->isEmpty(std::make_pair(right, position.second)))
	{
		return std::make_pair(right, position.second);
	}
	if (left >= worldPtr->getMinPos() && worldPtr->isEmpty(std::make_pair(left, position.second)))
	{
		return std::make_pair(left, position.second);
	}
	if (down <= worldPtr->getMaxPos() && worldPtr->isEmpty(std::make_pair(position.first, down)))
	{
		return std::make_pair(position.first, down);
	}
	if (up >= worldPtr->getMinPos() && worldPtr->isEmpty(std::make_pair(position.first, up)))
	{
		return std::make_pair(position.first, up);
	}
	return std::make_pair(-1, -1); //NIE MA WOLNEGO MIEJSCA -- W JAVIE JUZ ZROBILEM TU WYJATEK
}

void Organism::multiplicate()
{
	const auto spawnPosition = closeFreePos();
	if (spawnPosition.first == -1) { return; }			 //wystarczy jeden "-1"
	worldPtr->worldEvents()->addSpawnEvent(organismID);
	worldPtr->spawnOrganism(spawnPosition, getID());
}

bool operator==(const Organism& current, const Organism& other)
{
	return current.organismID == other.organismID;
} 