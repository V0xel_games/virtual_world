﻿#pragma once
#include <memory>
#include <vector>
#include "Organism.h"
#include "Logger.h"

class World
{
typedef std::shared_ptr<Organism>org_ptr;
public:
	World();
	~World();
	int getMaxPos() const;
	int getMinPos() const;
	Organism* getByPosition(std::pair<int, int> currPositon);	//Zwraca wskaznik bezposredni danego organizmu z vectora2D-mapy
	std::unique_ptr<Log::Logger>& worldEvents();

	void startSimualtion();
	void spawnOrganism(std::pair<int, int> position, const std::string& id);		//spawnuje organizm
	bool isEmpty(std::pair<int, int> position);										//sprawdza czy dana pozycja jest pusta
	void setNew2DPosition(std::pair<int, int> oldPos, std::pair<int, int> newPos);	//ustawia nowa pozycje w vectorze2D dla danego przez coordy organizmu 

private:
	std::vector<std::vector<org_ptr>>vector2D;		//2D zaincjalizowany na rozmiar vector pelniacy funkcje mapy
	std::vector<org_ptr>sortedOrganisms;			//wektor wskaznikow poosrtowanych wg inicjatywy
	std::vector<org_ptr>toAddOrganisms;				//wektor ktory pelni funkcje bufora, dla organiozmow ktore beda dodane do wektora posrtowanych
	std::unique_ptr<Log::Logger>events;
	void startNewTurn();
	void firstTurn();										
	void drawWorld();							
	void addOrganismInRandomPos(const std::string& id);	//dodaje organizm prosto z fabryki na losowe pole
	void cleanup();
	void saveToFile();
	void readFromFile();
	void printMenu() const;

	const int maxPos = 19;
	const int minPos = 0;
};
