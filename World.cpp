﻿#include "World.h"
#include "OrganismFactory.h"
#include <algorithm>
#include <limits>
#include "RandomNumberGenerator.h"
#include <fstream>
#undef min
#undef max //cause of WINAPI

World::~World() = default;
World::World() : vector2D(20, std::vector<org_ptr>(20, nullptr)), events(std::make_unique<Log::Logger>())
{}

int World::getMaxPos() const { return maxPos; }
int World::getMinPos() const { return minPos; }
std::unique_ptr<Log::Logger>& World::worldEvents() { return events; }

bool World::isEmpty(const std::pair<int, int> position)
{
	return vector2D[position.first][position.second] == nullptr;
}

Organism* World::getByPosition(const std::pair<int, int> currPositon)
{
	return vector2D[currPositon.first][currPositon.second].get();
}

void World::setNew2DPosition(const std::pair<int, int> oldPos, const std::pair<int, int> newPos)
{
	vector2D[newPos.first][newPos.second] = vector2D[oldPos.first][oldPos.second];
	vector2D[oldPos.first][oldPos.second] = nullptr;
}

void World::cleanup()
{
	sortedOrganisms.insert(sortedOrganisms.end(), toAddOrganisms.begin(), toAddOrganisms.end());	//z bufora na koniec posortowanego
	toAddOrganisms.clear();
	std::stable_sort(sortedOrganisms.begin(), sortedOrganisms.end(), [](const org_ptr a, const org_ptr b)	//stable pozwala na trzymanie organizmow po starosci turowej - kazdy kolejeny dodany bedzie po tych z wczesniejszej tury
	{
		return a->getInitiative() > b->getInitiative();
	});
	sortedOrganisms.erase(std::remove_if(sortedOrganisms.begin(), sortedOrganisms.end(), [](const org_ptr a) //erease remove idiom --no additional overhead
	{
		return a->isToDeletion();
	}), sortedOrganisms.end());
}

void World::startSimualtion() //WIP, Moznaby przeniesc do oddzielnej klasy
{
	char choice = 'r';
	while (choice =='r')
	{
		firstTurn();
		drawWorld();
		do
		{
			std::cin.get(choice);
			if (choice == 10)
			{
				startNewTurn();
				drawWorld();
			}
			else
			{
				std::cin.clear();
				std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
				if (choice == '2')
				{
					readFromFile();
					drawWorld();
				}
				else if (choice == '1')
				{
					saveToFile();
				}
			}
		} while (choice != 'q');
	}
}

void World::firstTurn()
{
	Console::Printer::getInstance().setTitle("Kacper Luczak, 167947");
	Console::Printer::getInstance().setWindow(78, 65);

	for (auto i = 0; i < 10; i++) 
	{
		addOrganismInRandomPos("Wolf");
		addOrganismInRandomPos("Sheep");
		addOrganismInRandomPos("Antelope");
		addOrganismInRandomPos("Mouse");
		addOrganismInRandomPos("Elephant");
		addOrganismInRandomPos("Grass");
		addOrganismInRandomPos("Belladona");
		addOrganismInRandomPos("Guarana"); 
	}
	cleanup();
}

void World::startNewTurn()
{
	for (const auto& element : sortedOrganisms)		//na kazdym ktore nie do usuniecia wywolujemy akcje
	{
		if (!element->isToDeletion())
		{
			element->action();
		}
	}
	cleanup();
}

void World::printMenu() const
{
	Console::Printer::getInstance().gotoxy(59, 1);
	std::cout << "Next turn: [ENTER]\n";
	Console::Printer::getInstance().gotoxy(59, 2);
	std::cout << "Save to file: 1\n";
	Console::Printer::getInstance().gotoxy(59, 3);
	std::cout << "Read from file: 2\n";
	Console::Printer::getInstance().gotoxy(59, 4);
	std::cout << "Quit: q\n";
}

void World::drawWorld()
{
	Console::Printer::getInstance().clrscr();
	Console::Printer::getInstance().gotoxy(0, 0);
	std::cout << *events;
	events->clear();
	printMenu();
	Console::Printer::getInstance().frame(Console::LIGHTBLUE, 0+35, 0, 21+35, 21);
	Console::Printer::getInstance().fillRect(219, Console::LIGHTGREEN, 1+35, 1, 20+35, 20);
	Console::Printer::getInstance().cursorOff();

	for (const auto& element : sortedOrganisms)
	{
		element->drawAtCurrentPos();
	}

	Console::Printer::getInstance().gotoxy(35, 23);
}

void World::addOrganismInRandomPos(const std::string& id)
{
	Rng::RandomNumberGenerator rng;

	while (true)
	{
		auto posX = rng.randomNumber(minPos, maxPos);
		auto posY = rng.randomNumber(minPos, maxPos);
		if (isEmpty(std::make_pair(posX, posY)))
		{
			spawnOrganism(std::make_pair(posX, posY), id);
			return;
		}
	}
}

void World::spawnOrganism(const std::pair<int, int> position, const std::string& id)
{
	org_ptr helper = Factory::OrganismFactory::getInstance().create(id, position.first, position.second);
	helper->setWorld(this);
	vector2D[position.first][position.second] = helper;		//dodajemy organizmy do vectora2D
	toAddOrganisms.push_back(helper);
	helper = nullptr;	//redutant
}

void World::saveToFile()
{
	std::ofstream file;
	file.open("save.txt", std::ios::out | std::ios::trunc);

	for (auto &org : sortedOrganisms)
	{
		file << org->getID() << " " << org->getPosition().first <<
			" " << org->getPosition().second << " " << org->getStrength() << "\n";
	}
	file.close();
}

void World::readFromFile()
{
	std::string id;
	std::ifstream file;
	int x, y, strength;
	file.open("save.txt");

	for (auto &i : vector2D)	//"czyszczenie" vectora2D
	{
		std::fill(i.begin(), i.end(), nullptr);
	}
	sortedOrganisms.clear();	//clean reszty kontenerow
	toAddOrganisms.clear();

	while (file >> id >> x >> y >> strength)
	{
		spawnOrganism(std::make_pair(x, y), id);
		vector2D[x][y]->setStrength(strength);
	}
	cleanup();
	file.close();
}


