#pragma once
#include <string>

namespace Log
{
	class Logger
	{
	public:
		Logger();
		~Logger();
		void addAttackWinEvent(const std::string& winner, const std::string& looser);
		void addDefenseWinEvent(const std::string& winner, const std::string& looser);
		void addEatEvent(const std::string& animal, const std::string& plant);
		void addSpawnEvent(const std::string& organism);
		void escapeEvent(const std::string& organism);
		void clear();
		friend std::ostream& operator<<(std::ostream& os, const Logger& data);
	private:
		std::string logData;
	};

}
