#pragma once
#include "Animal.h"
class Sheep : public virtual Animal
{
public:
	~Sheep() = default;
	Sheep(const int x, const int y) : Animal(x, y)
	{
		strength = 4;
		initiative = 4;
		organismID = "Sheep";
		symbol = 'S';
		color = Console::WHITE;
	}
};

