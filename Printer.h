#pragma once
#include <windows.h>
#include <string>
namespace Console
{
	enum COLORS //"enum class" nie moze byc bo WINAPi wtedy placze
	{
		BLACK = 0,
		BLUE,
		GREEN,
		CYAN,
		RED,
		MAGENTA,
		BROWN,
		LIGHTGRAY,
		DARKGRAY,     
		LIGHTBLUE,
		LIGHTGREEN,
		LIGHTCYAN,
		LIGHTRED,
		LIGHTMAGENTA,
		YELLOW,
		WHITE
	};

	//klasa obslugi konsoli - Singleton
	class Printer
	{
	public:
		static Printer& getInstance()
		{
			static Printer instance;
			return instance;
		}
		Printer(Printer const&) = delete;
		void operator=(Printer const&) = delete;
		void clrscr() const;	 //czysci ekran
		void frame(int attr, int xb, int yb, int xe, int ye) const;					//funkcja rysujaca ramke o danym kolorze
		void fillRect(TCHAR c, WORD attr, int xb, int yb, int xe, int ye) const;	//opis w implementacji
		void gotoxy(int x, int y) const;						//umiesza kursor na danej pozycji w konsoli
		void putxy(TCHAR c, WORD attr, int x, int y) const;		//umieszcza znak o danym kolorze na danej pozycji w konsoli
		void cursorOff() const;		//wy�acza kursor
		void cursorOn() const;		//w�acza kursor
		void setTextColor(int attr);			//ustawia kolor tekstu
		void setTextBackground(int attr);		//ustawia kolor tla
		void setTitle(const std::string& title) const; //ustawia tytul konsoli
		void setWindow(int width, int height) const;  //ustawia wielkosc bufora i okna konsoli
	private:
		Printer() {}
		HANDLE hcout = GetStdHandle(STD_OUTPUT_HANDLE);
		HANDLE hcin = GetStdHandle(STD_INPUT_HANDLE);
		WORD txattr = 7;
	};
}