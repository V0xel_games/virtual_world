#include "Printer.h"
namespace Console
{
	void Printer::clrscr() const //funkcja czyszczaca ekran
	{
		COORD cpos = { 0, 0 };
		DWORD cwrt;
		CONSOLE_SCREEN_BUFFER_INFO cinf;
		DWORD csize;

		if (!GetConsoleScreenBufferInfo(hcout, &cinf)) return;
		csize = cinf.dwSize.X * cinf.dwSize.Y;
		if (!FillConsoleOutputCharacter(hcout, ' ', csize, cpos, &cwrt)) return;
		if (!FillConsoleOutputAttribute(hcout, txattr, csize, cpos, &cwrt)) return;
		SetConsoleCursorPosition(hcout, cpos);
		SetConsoleTextAttribute(hcout, txattr);
	}

	void Printer::frame(const int attr, const int xb, const int yb, const int xe, const int ye) const //od lewego gornego rogu do prawego
	{
		if ((xb >= xe) || (yb >= ye)) return; //gdy zle wspolrzedne
		unsigned short c = 219; //kod znaku ASCII
		fillRect(c, attr, xb, yb, xb, yb); //lewy gorny rog
		c = 219;
		fillRect(c, attr, xb, ye, xb, ye); //lewy dolny rog
		c = 219;
		fillRect(c, attr, xe, yb, xe, yb); //prawy gorny rog
		c = 219;
		fillRect(c, attr, xe, ye, xe, ye); //prawy dolny rog
		c = 220;
		fillRect(c, attr, xb + 1, yb, xe - 1, yb); //gora
		c = 223;
		fillRect(c, attr, xb + 1, ye, xe - 1, ye); //dol
		c = 219;
		fillRect(c, attr, xb, yb + 1, xb, ye - 1); //lewo
		fillRect(c, attr, xe, yb + 1, xe, ye - 1); //prawo
	}

	// Funkcja wype�nia dany obszar okna konsoli podanym znakiem i atrybutem. Pozycja kursora nie jest zmieniana.
	//----------------------------------------------
	// c - znak do wype�nienia
	// attr - atrybut do wype�nienia obszaru
	// xb,yb - wsp�rz�dne lewego g�rnego naro�nika
	// xe,ye - wsp�rz�dne prawego dolnego naro�nika
	//----------------------------------------------
	void Printer::fillRect(const TCHAR c, const WORD attr, int xb, int yb, int xe, int ye) const
	{
		CONSOLE_SCREEN_BUFFER_INFO cinf;
		COORD pos;
		DWORD crd;

		if (!GetConsoleScreenBufferInfo(hcout, &cinf)) return;
		if ((xb >= cinf.dwSize.X) || (yb >= cinf.dwSize.Y) ||
			(xe < xb) || (ye < yb) || (xe < 0) || (ye < 0)) return; // poza oknem
		if (xb < 0) xb = 0;
		if (yb < 0) yb = 0;
		if (xe >= cinf.dwSize.X) xe = cinf.dwSize.X - 1;
		if (ye >= cinf.dwSize.Y) ye = cinf.dwSize.Y - 1;
		const DWORD len = xe - xb + 1;
		pos.X = xb;
		for (int i = yb; i <= ye; i++)
		{
			pos.Y = i;
			FillConsoleOutputCharacter(hcout, c, len, pos, &crd);
			FillConsoleOutputAttribute(hcout, attr, len, pos, &crd);
		}
	}

	void Printer::gotoxy(const int x, const int y) const
	{
		COORD cpos = { x,y };
		SetConsoleCursorPosition(hcout, cpos);
	}

	void Printer::putxy(TCHAR c, WORD attr, const int x, const int y) const
	{
		COORD pos = { x,y };
		DWORD cwrt;
		WriteConsoleOutputCharacter(hcout, &c, 1, pos, &cwrt);
		WriteConsoleOutputAttribute(hcout, &attr, 1, pos, &cwrt);
	}

	void Printer::cursorOff() const
	{
		CONSOLE_CURSOR_INFO ccinf;

		GetConsoleCursorInfo(hcout, &ccinf);
		ccinf.bVisible = false;
		SetConsoleCursorInfo(hcout, &ccinf);
	}

	void Printer::cursorOn() const
	{
		CONSOLE_CURSOR_INFO ccinf;

		GetConsoleCursorInfo(hcout, &ccinf);
		ccinf.bVisible = true;
		SetConsoleCursorInfo(hcout, &ccinf);
	}

	void Printer::setTextColor(const int attr)
	{
		txattr = attr & 0xff;
		SetConsoleTextAttribute(hcout, txattr);
	}

	void Printer::setTextBackground(const int attr)
	{
		txattr = (txattr & 0xf) | ((attr & 0xf) << 4);
		SetConsoleTextAttribute(hcout, txattr);
	}

	void Printer::setTitle(const std::string& title) const
	{
		SetConsoleTitle(title.c_str());
	}

	void Printer::setWindow(const int width, const int height) const
	{
		_COORD coord;
		coord.X = width;
		coord.Y = height;
		_SMALL_RECT rect;
		rect.Top = 0;
		rect.Left = 0;
		rect.Bottom = height - 1;
		rect.Right = width - 1;
		SetConsoleScreenBufferSize(hcout, coord);
		SetConsoleWindowInfo(hcout, TRUE, &rect);
	}
}
