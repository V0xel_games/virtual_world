#pragma once
#include "Plant.h"
class Grass : public Plant
{
public:
	~Grass() = default;
	Grass(const int x, const int y)	: Plant(x, y)
	{
		organismID = "Grass";
		symbol = 'G';
		color = Console::GREEN;
	}
};

