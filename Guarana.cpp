#include "Guarana.h"
#include "OrganismFactory.h"
static Factory::Registrar<Guarana> REGISTRAR("Guarana");

void Guarana::collision(const bool isAttacking, Organism* other)
{
	other->increaseStrength(strengthBoost);
	Plant::collision(isAttacking, other);
}
