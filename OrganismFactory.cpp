﻿#include "OrganismFactory.h"

namespace Factory
{
	std::shared_ptr<Organism> OrganismFactory::create(const std::string& id, int x, int y)
	{
		Organism* instance = nullptr;

		// znalezienie id i call funkcji
		auto it = factoryRegistry.find(id);
		if (it != factoryRegistry.end())
			instance = it->second(x, y);		

		// wrappowanie do shared ptr
		if (instance != nullptr)
			return std::shared_ptr<Organism>(instance);
		else
			return nullptr;
	}

	void OrganismFactory::registerFunc(const std::string& id, std::function<Organism*(int, int)> classFactoryFunction)
	{
		factoryRegistry[id] = classFactoryFunction;
	}

	OrganismFactory::OrganismFactory()
	{
	}
}