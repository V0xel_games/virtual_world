#pragma once
#include "Plant.h"
class Guarana : public Plant
{
public:
	~Guarana() = default;
	Guarana(const int x, const int y) : Plant(x, y), strengthBoost(3)
	{
		organismID = "Guarana";
		symbol = 'U';
		color = Console::LIGHTRED;
	}
	
protected:
	void collision(bool isAttacking, Organism* other) override;

private:
	int strengthBoost;
};

