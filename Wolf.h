#pragma once
#include "Animal.h"
class Wolf : public virtual Animal 
{
public:
	~Wolf() = default;
	Wolf(const int x, const int y) : Animal(x, y)
	{
		strength = 9;
		initiative = 5;
		organismID = "Wolf";
		symbol = 'W';
		color = Console::DARKGRAY;
	}
};

