﻿#pragma once
#include <memory>
#include <functional>
#include <map>
#include "Organism.h"

namespace Factory
{
	class OrganismFactory
	{
	public:
		template<class T> friend class Registrar;
		static OrganismFactory& getInstance() //Singleton
		{
			static OrganismFactory instance;
			return instance;
		}
		OrganismFactory(OrganismFactory&) = delete;
		void operator=(OrganismFactory const&) = delete;
		std::shared_ptr<Organism>create(const std::string& id, int x, int y);
	private:
		void registerFunc(const std::string& id, std::function<Organism*(int, int)> classFactoryFunction);
		std::map<std::string, std::function<Organism*(int, int)>> factoryRegistry;
		OrganismFactory();
	};

	template<class T> //pomocnicza klasa rejestrujaca
	class Registrar
	{
	public:
		Registrar(std::string className)
		{
			OrganismFactory::getInstance().registerFunc(className,
				[](int x, int y) -> Organism* { return new T(x, y); });
		}
	};
}