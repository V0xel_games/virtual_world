#pragma once
#include "Animal.h"
class Mouse : public Animal
{
public:
	~Mouse() = default;
	Mouse(const int x, const int y) : Animal(x, y)
	{
		strength = 1;
		initiative = 6;
		organismID = "Mouse";
		symbol = 'M';
		color = Console::LIGHTGRAY;
	}

protected:
	void collision(bool isAttacking, Organism* other) override;
};

